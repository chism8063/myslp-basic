<?php
/**
 * MySLP functions and definitions
 *
 * @package MySLP
 * @subpackage Basic
 * @since 1.0
 *
 * Text Domain: myslp-basic
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * Include the base object for managing this theme
 *
 * @var theme_MySLP_Basic  $myslp_basic_theme
 */
require_once get_template_directory() . '/inc/theme_MySLP_Basic.php';
$myslp_basic_theme = $GLOBALS['theme_MySLP_Basic'];



