<?php
/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MySLP
 * @subpackage Basic
 * @since 1.0
 *
 * Text Domain: myslp-basic
 */
?>

		</div><!-- #content .site-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="site-info">
				<span class="footer-attribution"><a href="<?php echo esc_url( __( 'http://www.cybersprocket.com/', 'myslp-basic' ) ); ?>"><?php printf( __( 'A %s creation.', 'myslp-basic' ), 'Cyber Sprocket Labs' ); ?></a></span>
				<span class="footer-copyright"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
			</div><!-- .site-info -->
		</footer><!-- .site-footer -->

	<?php

	wp_footer();

	?>
	</body>
</html>
