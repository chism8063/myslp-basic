<?php
/**
 * The main template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MySLP
 * @subpackage Basic
 * @since 1.0
 *
 * Text Domain: myslp-basic
 *
 */

get_header();
?>
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<?php

					if ( have_posts() ) :

						if ( is_home() && ! is_front_page() ) :
							?>
							<header>
								<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
							</header>
							<?php
						endif;

						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/content', get_post_format() );
						endwhile;

						the_posts_pagination( array(
							'prev_text'          => __( 'Previous page', 'myslp-basic' ),
							'next_text'          => __( 'Next page', 'myslp-basic' ),
							'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'myslp-basic' ) . ' </span>',
						) );

					else :
						get_template_part( 'template-parts/content', 'none' );

					endif;

					?>

				</main><!-- #main .site-main -->
			</div><!-- #primary .content-area -->
<?php
get_footer();

