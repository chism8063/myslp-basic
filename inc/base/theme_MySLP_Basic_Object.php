<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'theme_MySLP_Basic_Object' ) ) :

	/**
	 * Class theme_MySLP_Basic_Object
	 *
	 * @package MySLP
	 * @subpackage Basic
	 * @since 1.0
	 *
	 * Text Domain: myslp-basic
	 *
	 * @var     theme_MySLP_Basic   $theme
	 */
	class theme_MySLP_Basic_Object {
		protected   $theme;

		/**
		 * @param array $options
		 */
		function __construct( $options = array() ) {

			if ( is_array( $options ) && ! empty( $options ) ) {
				foreach ( $options as $property => $value ) {
					if ( property_exists( $this, $property ) ) {
						$this->$property = $value;
					}
				}
			}
			$this->initialize();
		}

		/**
		 * Do these things when this object is invoked.
		 */
		protected function initialize() {
			// Override with anything you want to run when your extension is invoked.
		}
	}

endif;

