<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'theme_MySLP_Basic_Object_WithOptions' ) ) :
	require_once get_template_directory() . '/inc/base/theme_MySLP_Basic_Object.php';

	/**
	 * Class theme_MySLP_Basic_Object_WithOptions
	 *
	 * @package MySLP
	 * @subpackage Basic
	 * @since 1.0
	 *
	 * Text Domain: myslp-basic
	 *
	 *
	 * @var   string $class_prefix  the prefix that goes before all our classes
	 *
	 * @var   string  $dir          the root directory for this theme
	 *
	 * @var   string  $option_name  the name of the wp_option option_name we are using
	 *
	 * @var   array   $options      key = option slug, array of attributes()
	 *                                  value = the value of the option
	 *
	 * @var   array   $objects      key = class name, array of attributes()
	 *                                  object = the instantiated object
	 *                                  subdir = the subdirectory (from theme root) that contains the class definition
	 */
	class theme_MySLP_Basic_Object_WithOptions extends theme_MySLP_Basic_Object {
		protected $class_prefix;
		public $dir;
		protected $option_name;
		protected $options = array();
		protected $objects = array();

		/**
		 * Get the value, running it through a filter.
		 *
		 * @param string $property
		 *
		 * @return mixed     null if not set or the value
		 */
		function __get( $property ) {

			// Option
			if ( array_key_exists( $property , $this->options ) ) {
				return $this->options[ $property ]['value'];
			}

			// Object
			if ( array_key_exists( $property , $this->objects ) ) {
				return $this->objects[ $property ]['object'];
			}

			return null;
		}

		/**
		 * Allow isset to be called on private properties.
		 *
		 * @param $property
		 *
		 * @return bool
		 */
		public function __isset( $property ) {

			// option is set
			if ( array_key_exists( $property , $this->options ) && isset( $this->options[ $property ]['value'] ) ) {
				return true;
			}

			// object is set
			if ( array_key_exists( $property , $this->objects ) && is_object( $this->objects[ $property ]['object'] ) ) {
				return true;
			}

			return false;
		}

		/**
		 * Allow value to be set directly.
		 *
		 * @param $property
		 *
		 * @param $value
		 * @return SLP_Option
		 */
		public function __set( $property, $value ) {
			if ( array_key_exists( $property , $this->options ) ) {
				$this->options[ $property ]['value'] = $value;
				return $this->options[ $property ];
			}
		}

		/**
		 * Get an option attribute.
		 *
		 * @param string $property
		 * @param string $attribute
		 *
		 * @return mixed
		 */
		public function get_option( $property , $attribute ) {
			if ( array_key_exists( $property , $this->options ) && isset( $this->options[ $property ][$attribute] ) ) {
				return $this->options[ $property ][$attribute];
			}
			return null;
		}

		/**
		 * Grab the specified option from wp_options and put it on top of any default values we've got.
		 */
		public function load_options() {
			if ( empty( $this->option_name ) ) {
				return;
			}
			$wp_options = get_option( $this->option_name , array() );
			foreach ( $wp_options as $property => $value ) {
				$this->$property = $value;
			}
		}

		/**
		 * Set an option attribute.
		 *
		 * @param string $property
		 * @param string $attribute
		 * @param mixed $value
		 *
		 * @return bool
		 */
		public function set_option( $property , $attribute , $value ) {
			if ( array_key_exists( $property , $this->options ) ) {
				$this->options[ $property ][$attribute] = $value;
				return true;
			}
			return false;
		}

		/**
		 * Augment the class names so we can reference simple property names.
		 *
		 * @param string $class
		 *
		 * @return string
		 */
		private function augment_class( $class ) {
			return $this->class_prefix . $class;
		}

		/**
		 * Set the dir property.
		 */
		protected function initialize() {
			$this->dir = get_template_directory();
		}

		/**
		 * Instantiate an object of the noted class.
		 *
		 * @param string $class
		 *
		 * @return null|object
		 */
		public function instantiate( $class ) {
			if ( ! array_key_exists( $class, $this->objects ) ) {
				return null;
			}
			if ( ! isset( $this->$class ) ) {
				$full_class = $this->augment_class( $class );
				include_once( $this->dir . '/' . $this->objects[$class]['subdir'] . $full_class .'.php' );
				if ( ! class_exists( $full_class ) ) {
					return null;
				}
				$this->objects[ $class ]['object'] = new $full_class( array( 'theme' => $this ) );
			}
			return $this->class;
		}
	}

endif;

