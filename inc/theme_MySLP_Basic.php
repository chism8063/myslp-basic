<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'theme_MySLP_Basic' ) ) :
	require_once get_template_directory() . '/inc/base/theme_MySLP_Basic_Object_WithOptions.php';

	/**
	 * Class theme_MySLP_Basic
	 *
	 * @package    MySLP
	 * @subpackage Basic
	 * @since      1.0
	 *
	 * Text Domain: myslp-basic
	 *
	 * @var array $meta things we want to track to make this theme somewhat generic.
	 */
	class theme_MySLP_Dashboard extends theme_MySLP_Basic_Object_WithOptions {
		private $meta = array(
			'slug' => 'myslp-basic',
		);

		/**
		 * Things we do at the start.
		 */
		public function initialize() {
			$this->add_hooks();
		}

		/**
		 * Setup hooks.
		 */
		private function add_hooks() {
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_js_and_css' ) );
		}

		/**
		 * Enqueues scripts and styles.
		 */
		function enqueue_js_and_css() {
			wp_enqueue_style( $this->meta['slug'] . '-style', get_stylesheet_uri() );  // The main stylesheet
		}
	}

	$GLOBALS['theme_MySLP_Basic'] = new theme_MySLP_Dashboard();

endif;