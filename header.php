<?php
/**
 * The template for displaying the header
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MySLP
 * @subpackage Basic
 * @since 1.0
 *
 * Text Domain: myslp-basic
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php

		if ( is_singular() && pings_open( get_queried_object() ) ) :
			?>
			<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
			<?php
		endif;

		wp_head();

		?>
	</head>

	<body <?php body_class(); ?>>
		<div id="content" class="site-content">
